﻿<?php
	require_once(CHEMIN_LIB."mail.class.php");

	function EmailInscriptionOk($email, $hash_validation){
		$message_mail = '<html><head></head><body>
					<p>Merci de vous être inscrit sur "Moxx & Co" !</p>
					<p>Veuillez cliquer sur <a href="'.$_SERVER['SERVER_NAME'].'?module=membres&amp;action=valider_compte&amp;hash='.$hash_validation.'">ce lien</a> pour activer votre compte !</p>
					<br/><br/><p align="right">L\'équipe "Moxx & Co"</p><br/>
					</body></html>';
		
		$Mail = new CMail();
		$Mail->from     = "noreply@moxx-et-co.com";
		$Mail->fromName = "Moxx & Co";
		$Mail->to       = array($email);
		$Mail->subject  = "Inscription sur <moxx-et-co.com>";
		$Mail->message  = $message_mail;
		$Mail->charset  = "utf-8";
		$Mail->mime     = "text/html";
		
		// Envoi du mail
		$Mail->Send(); 
	}
	
	function EmailPostOk($email){
		$message_mail = '<html><head></head><body>
					<p>Le contenu que vous venez de poster sera très vite mis en ligne !</p>
					<br/><p>Merci de votre participation.</p> 
					</body></html>';
		
		$Mail = new CMail();
		$Mail->from     = "noreply@dtour30.com";
		$Mail->fromName = "Passion Rugby";
		$Mail->to       = array($email);
		$Mail->subject  = "Post d'un contenu";
		$Mail->message  = $message_mail;
		$Mail->charset  = "utf-8";
		$Mail->mime     = "text/html";
		
		// Envoi du mail
		$Mail->Send(); 
	}
?>