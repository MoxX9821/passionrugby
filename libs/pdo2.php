<?php

	/**
	 * Classe impl�mentant le singleton pour PDO
	 */
	class PDO2 {

		private static $_instance;

		/* Constructeur : h�ritage public obligatoire par h�ritage de PDO */
		public function __construct( ) {
		
		}
		// End of PDO2::__construct() */

		/* Singleton */
		public static function getInstance() {
		
			if (!isset(self::$_instance)) {
				
				try {
				
					self::$_instance = new Mysql(SQL_SERVER, SQL_DB, SQL_USERNAME, SQL_PASSWORD);
				
				} catch (MySQLExeption $e) {
				
					echo $e->RetourneErreur();
				}
			} 
			return self::$_instance; 
		}
		// End of PDO2::getInstance() */
	}

	// end of file */
?>