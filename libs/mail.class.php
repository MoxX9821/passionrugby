<?php

// --- classe pour envoyer des emails
// avec gestion des pi�ces jointes

define("BOUNDARY", "--".md5(rand()));

class CMail {
  public $from;        // votre email
  public $fromName;    // votre nom
  public $to;          // destinataire
  public $cc;          // copie �
  public $bcc;         // copie cach�e �
  public $subject;     // sujet du mail
  public $priority;    // priorit� 1-5
  public $returnPath;  // email utilis� pour la r�ponse
  public $notify;      // email pour notification
  public $message;     // texte du mail
  public $charset;     // tjeu de caract�res, iso-8859-1 par d�faut
  public $mime;        // type mime, text/plain par d�faut
  public $debug;       // affichage ou non des erreurs
  public $debug_txt;   // messages d'erreurs
  
  protected $body;
  protected $header;
  protected $attachments = Array();

  // Email priorities  
  protected $priorities = Array(
    '1 (Highest)',
    '2 (High)',
    '3 (Normal)',
    '4 (Low)',
    '5 (Lowest)');
  
  
  // --- constructeur
  public function __construct() {
    $this->clear();
  }

  // --- valeurs par d�faut
  public function clear() {
    $this->mime        = "text/plain";
    $this->message     = "";
    $this->charset     = "utf-8";
    $this->from        = "noreply@moxx-et-co.com";
    $this->fromName    = "Moxx & Co webmaster";
    $this->to          = "";
    $this->cc          = "";
    $this->bcc         = "";
    $this->subject     = "";
    $this->returnPath  = "";
    $this->notify      = "";
    $this->priority    = 0;
    $this->debug       = false;
    $this->clearAttachments();
  }

  // --- v�rifie si la syntaxe d'une adresse email est valide
  public static function email_ok($email) {
    return eregi("^([-!#\$%&'*+./0-9=?A-
Z^_`a-z{|}~])+@([-!#\$%&'*+/0-9=?A-
Z^_`a-z{|}~]+\\.)+[a-zA-Z]{2,6}\$", $email) != 0;
  }

  // --- retourne le type MIME d'un fichier
  public static function getMimeType($file) {
    static $mimeTypes = Array(
      '.gif'  => 'image/gif',
      '.jpg'  => 'image/jpeg',
      '.jpeg' => 'image/jpeg',
      '.jpe'  => 'image/jpeg',
      '.bmp'  => 'image/bmp',
      '.png'  => 'image/png',
      '.tif'  => 'image/tiff',
      '.tiff' => 'image/tiff',
      '.swf'  => 'application/x-shockwave-flash',
      '.doc'  => 'application/msword',
      '.xls'  => 'application/vnd.ms-excel',
      '.ppt'  => 'application/vnd.ms-powerpoint',
      '.pdf'  => 'application/pdf',
      '.ps'   => 'application/postscript',
      '.eps'  => 'application/postscript',
      '.rtf'  => 'application/rtf',
      '.bz2'  => 'application/x-bzip2',
      '.gz'   => 'application/x-gzip',
      '.tgz'  => 'application/x-gzip',
      '.tar'  => 'application/x-tar',
      '.zip'  => 'application/zip',
      '.rar'  => 'application/rar',
      '.js'   => 'text/javascript',
      '.html' => 'text/html',
      '.htm'  => 'text/html',
      '.txt'  => 'text/plain',
      '.css'  => 'text/css'
    );

    $att = StrRChr(StrToLower($file), ".");
    if(!IsSet($mimeTypes[$att]))
      return "application/octet-stream";
    else
      return $mimeTypes[$att];
  }

  // --- supprime les pi�ces jointes
  public function clearAttachments() {
    $this->attachments = Array();
  }

  // --- ajout d'une pi�ce jointe encod�e en base64
  // @param $filename     nom du fichier sur le serveur
  // @param $inner_name   nom du fichier affich� dans l'email
  // @param $mime         type mime
  public function addAttachment($filename, $inner_name="", $mime="") {
    if(!file_exists($filename))
      $this->debug_txt .= "Fichier filename non trouv�";

    if(!is_readable($filename))
      $this->debug_txt .= "Fichier filename inaccessible";

    $fp = @fopen($filename, "r");
    if(!$fp)
      $this->debug_txt .= "Impossible d'ouvrir le fichier $filename";

    // --- nom de fichier � afficher non pr�cis�, on
    // prend le nom du fichier ou est stock�e la pi�ce jointe
    if($inner_name == "")
      $inner_name = basename($filename);

    // --- type mime non pr�cis�, on le d�termine �
    // partir du nom du fichier
    if($mime == "")
      $mime = $this->getMimeType($inner_name);

    $attachment = "";

    $attachment .= "\n\n--".BOUNDARY."\n";
    $attachment .= "Content-Transfer-Encoding: base64\n";
    $attachment .= "Content-Type: $mime; name=\"".$inner_name.
    "\"; charset=\"us-ascii\"\n";
    $attachment .= "Content-Disposition: attachment; 
    filename=\"".$inner_name."\"\n\n";
    $attachment .= chunk_split(base64_encode(@fread($fp, @filesize($filename))));

    array_push($this->attachments, $attachment);

    @fclose($fp);
  }

  // --- envoi du mail
  /* 
  @param $emailfile si sp�cifi�, 
  le mail sera �galement envoy� en pi�ce jointe
  */
  public function Send($emailfile = "") {
	
    $this->body        = "";
    $this->header      = "";

    if(strlen($this->from))
      if(!$this->email_ok($this->from))
        $this->debug_txt .= "From: ".$this->from." n'est pas un email valide";

    if(strlen($this->returnPath)) {
      if(!$this->email_ok($this->returnPath))
        $this->debug_txt .= "Return Path ".$this->returnPath." 
        n'est pas un email valide";
      $this->header .= "Return-path: <".$this->returnPath.">\n";
    }

    if(strlen($this->from))
      $this->header .= "From: ".$this->fromName." <".$this->from.">\n";

    $ok = $this->email_ok($this->to);
    if(!$ok) $this->debug_txt .= "Email To: $$this->to n'est pas un email valide";

    if(!Empty($this->cc)) {
      $ok = $this->email_ok($this->cc);
      if(!$ok) $this->debug_txt .= "Email Cc: $invalidEmail n'est pas valide";
      $this->header .= "Cc: ";
      $this->header .= is_array($this->cc) ? implode(", ", $this->cc) : $this->cc;
      $this->header .= "\n";
    }
      
    if(!Empty($this->bcc)) {
      $ok = $this->email_ok($this->bcc);
      if(!ok) $this->debug_txt .= "Email Bcc: $invalidEmail  n'est pas valide";
       $this->header .= "Bcc: ";
      $this->header .= is_array($this->bcc) ? implode(", ", $this->bcc) : $this->bcc;
      $this->header .= "\n";
    }

    $this->header .= "Mime-Version: 1.0\n";
    
    if(IntVal($this->notify) == 1)
      $this->header .= "Disposition-Notification-To: <".$this->from.">\n";
    else if(strlen($this->notify))
      $this->header .= "Disposition-Notification-To: <".$this->notify.">\n";
      
    if(!Empty($this->attachments)) {
      // header with attachments
     $this->header .= "Content-Type: multipart/mixed; boundary=\"".BOUNDARY."\"\n";
     $this->header .= "Content-Transfer-Encoding: 7bit\n";
     $this->body   .= "This is a multi-part message in MIME format.\n\n";
    }
    else {
     // header with no attachments
     $this->header .= "Content-Transfer-Encoding: 8bit\n";
     $this->header .= "Content-Type: ".$this->mime."; charset=\"".$this->charset."\"".(Empty($emailfile) ? "" : " name=\"$emailfile\"")."\n";
     $this->body   .= $this->message;
    }

    if($this->priority)
     $this->header .= "X-Priority: ".$this->priorities[$this->priority]."\n";
    
    if(!Empty($this->attachments)) {
     $this->body .= "\n\n--".BOUNDARY."\n";
     $this->body .= "Content-Transfer-Encoding: 8bit\n";
     $this->body .= "Content-Type: ".$this->mime."; charset=\"".$this->charset."\"".(Empty($emailfile) ? "" : " name=\"$emailfile\"")."\n";
     $this->body .= "Mime-Version: 1.0\n\n";
     $this->body .= $this->message."\n\n";

     reset($this->attachments);
     while(list($key, $attachment) = each($this->attachments)) {
       $this->body .= $attachment;
     }
      
     // --- fin du mail
     $this->body .= "\n\n--".BOUNDARY."--";
   }
    
   // --- texte pour deboguage
   if($this->debug) {
     echo "<pre>";
     echo "\nTO\n".HTMLSpecialChars($this->to);
     echo "\nSUBJECT\n".HTMLSpecialChars($this->subject);
     echo "\nBODY\n".HTMLSpecialChars($this->body);
     echo "\nHEADER\n".HTMLSpecialChars($this->header);
     echo "</pre>";
   }

   // --- envoi � plusieurs personnes si le
   // param�tre est un tableau
   if(is_array($this->to)) {
     reset($this->to);
     while(list($key, $val) = each($this->to)) {
       $this->sendTo($val);
     }
   }
   else {
     $this->sendTo($this->to);
   }
 }
  
 protected function sendTo($to) {
   if(!@mail($to, $this->subject, $this->body, $this->header))
     $this->debug_txt .= "PHP::Mail() Erreur d'envoi du mail $to";
 } 
}
?>