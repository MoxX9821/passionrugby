function addPost() {
		var hasError = false;
		if(document.getElementById("postNom").value == "") {
			document.getElementById("postNom").className = document.getElementById("postNom").className + " error";
			hasError = true;
		} else {
			document.getElementById("postNom").className = document.getElementById("postNom").className.replace(" error", "");
		}
		
		email = document.getElementById("postEmail").value;
		if(email == "" || !validateEmail(email)) {
			document.getElementById("postEmail").className = document.getElementById("postNom").className + " error";
			hasError = true;
		} else {
			document.getElementById("postEmail").className = document.getElementById("postNom").className.replace(" error", "");
		}
		
		if(document.getElementById("postLibelle").value == "") {
			document.getElementById("postLibelle").className = document.getElementById("postNom").className + " error";
			hasError = true;
		} else {
			document.getElementById("postLibelle").className = document.getElementById("postNom").className.replace(" error", "");
		}
		
		if(document.getElementById("postMedia").value == "") {
			document.getElementById("postMedia").className = document.getElementById("postNom").className + " error";
			hasError = true;
		} else {
			document.getElementById("postMedia").className = document.getElementById("postNom").className.replace(" error", "");
		}
		
		if(hasError)
			return;
		else 
			document.getElementById("postForm").submit();
	}
	
	function validateEmail(email) { 
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	} 