﻿<?php
	$saisonId = $_GET['saison'];
	$isAdmin = isset($_GET['admin']) && $_GET['admin'] == "dtourreau";
	
	if(isset($_POST["postNom"])) {
		$nom = $_POST["postNom"];
		$email = $_POST["postEmail"];
		$libelle = $_POST["postLibelle"];
		$file = $_POST["postMedia"];
		$typeMedia = $_POST["postTypeMedia"];
		
		//print_r($_POST);
		
		$chemin = DOSSIER_UPLOAD.$_FILES['postMedia']['name'];
		move_uploaded_file($_FILES['postMedia']['tmp_name'], $chemin);
		
		$nb = ajouterMedia($nom, $email, $libelle, $chemin, $typeMedia, $saisonId);
		
		if($nb > 0) {
			echo '<h3 style="color:green">Votre contenu a été ajouté</h3>';
		} else {
			echo '<h3 style="color:red">Une erreur s\'est produite lors du transfert du contenu</h3>';
		}
	}
	
	if(isset($_POST["statutMediaId"])) {
		$visible = isset($_POST["statutMedia"]) ? 1 : 0;
		$mediaId = $_POST["statutMediaId"];
		$nb = updateMediaStatus($mediaId, $visible);
		
		if($nb > 0) {
			echo '<h3 style="color:green">Le changement de statut est effectif</h3>';
		} else {
			echo '<h3 style="color:red">Une erreur s\'est produite lors du changement de statut</h3>';
		}
	}
?>

<div style="text-align: right;">
	<button onclick="javascript:openDialog('addContent');">Poster du contenu</button>
	<aside class="avgrund-popup undefined" id="addContent">
		<div style="text-align: right;">
			<a href="javascript:closeDialog();">
				<img src="images/close.png" alt="Fermer" />
			</a>
		</div>
		<form id="postForm" enctype="multipart/form-data" method="post" action="">
			<h3>Ajout de contenu</h3>
			<table style="margin-left:20px">
				<tr>
					<td style="text-align:right">
						Nom : 
					</td>
					<td>
						<input id="postNom" name="postNom" type="text" style="height:25px; width=250px" />
					</td>
				</tr>
				<tr>
					<td style="text-align:right">
						Email : 
					</td>
					<td>
						<input id="postEmail" name="postEmail" type="text" style="height:25px; width=250px" />
					</td>
				</tr>
				<tr>
					<td style="text-align:right">
						Libell&eacute; : 
					</td>
					<td>
						<input id="postLibelle" name="postLibelle" type="text" style="height:25px; width=250px" />
					</td>
				</tr>
				<tr>
					<td style="text-align:right">
						M&eacute;dia : 
					</td>
					<td>
						<input id="postMedia" name="postMedia" type="file" style="height:25px; width=250px" />
					</td>
				</tr>
				<tr>
					<td>
						Type de média : 
					</td>
					<td>
						<input type="radio" id="photo" value="1" name="postTypeMedia" checked="checked" /> Photo
						<input type="radio" id="video" value="0" name="postTypeMedia" style="margin-left:15px" /> Vidéo
					</td>
				</tr>
			</table>
			
			<div style="text-align:center"><a href="javascript:addPost();">Valider</a></div>
		</form>
	</aside>
</div>

<div class="joomla">
	<div class="blog">
		<div class="leadingarticles">
<?php
	$medias = RecupMediasBySaisonId($saisonId);
	
	if($medias) {
		for($i=0; $i<sizeof($medias); $i++) {
			$media = $medias[$i];
			
			if($media["visible"] == 1 || $isAdmin) {
				$statut = $media["visible"] == 0 ? "" : "checked=\"checked\"";
?>
			<article class="avgrund-contents">
				<div class="item">
					<div class="item-bg">
<?php
				if($media["type"] == 1) {
?>
						<img class="media" src="<?php echo $media["chemin"] ?>" alt="<?php echo $media["libelle"] ?>" width="400" height="300" />
<?php
				} else {
?>
						<video class="media" controls src="<?php echo $media["chemin"] ?>"><?php echo $media["libelle"] ?></video>
<?php
				}
?>
						<div>
							<h4><?php echo utf8_encode($media["libelle"]) ?><h4>
						</div>
						<button onclick="javascript:openDialog('<?php echo 'default-popup-'.$media["id"] ?>');"><?php echo $media["nbComm"] ?> commentaire(s)</button>
					</div>
<?php
				if($isAdmin) {
?>					
					<form name="changeStatut" method="post" action="">
						<input type="checkbox" name="statutMedia" <?php echo $statut ?>> Visible
						<input type="hidden" name="statutMediaId" value="<?php echo $media["id"] ?>" />
						<button>Changer le statut</button>
					</form>
<?php
				}
?>
				</div>
			</article>

<?php
				// récupération des commentaires
				$commentaires = RecupCommentsByMediaId($media["id"]);
?>
			<aside class="avgrund-popup undefined" id="<?php echo 'default-popup-'.$media["id"] ?>">
				<div style="text-align: right;">
					<a href="javascript:closeDialog();">
						<img src="images/close.png" alt="Fermer" />
					</a>
				</div>
<?php
				for($j=0; $j<sizeof($commentaires); $j++) {
					if($j > 0) {
						echo '<br/><hr/><br/>';
					}
?>
				<p>
					<?php echo $commentaires[$j]["commentaire"] ?>
					<br/>
					<p class="articleinfo">
						<span class="created">Post&eacute; par <?php echo $commentaires[$j]["NomUtil"] ?> le ...</span>
					</p>
				</p>
<?php
				}
?>
			</aside>
<?php
			}
		}
	} else {
		echo '<h3>Cette saison n\'a pas encore de contenu associé</h3>';
	}
?>

		</div>
	</div>
</div>