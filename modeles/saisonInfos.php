<?php
	function RecupMediasBySaisonId($saisonId) {
		try
		{
			$connection = new Mysql(SQL_SERVER, SQL_DB, SQL_USERNAME, SQL_PASSWORD);
			
			$sql  = "SELECT ";
			$sql .= " id, libelle, chemin, type, saisonId, visible, (SELECT COUNT(*) FROM Commentaire WHERE mediaId = Media.id) AS nbComm ";
			$sql .= "FROM Media ";
			$sql .= "WHERE saisonId = ".$saisonId;
			
			if($connection->IsDebug())
			{
				echo $sql;
			}
			
			$results = $connection->TabResSQL($sql);
			
			if(sizeof($results) > 0)
				return $results;
			else
				return false;
		}
		catch (MySQLExeption $e)
		{
			return $e->RetourneErreur();
		}
	}
	
	function RecupCommentsByMediaId($mediaId) {
		try
		{
			$connection = new Mysql(SQL_SERVER, SQL_DB, SQL_USERNAME, SQL_PASSWORD);
			
			$sql  = "SELECT T1.id, T1.commentaire, T2.id, T2.nom AS NomUtil ";
			$sql .= "FROM Commentaire T1 INNER JOIN Utilisateur T2 ON T1.utilisateurId = T2.id ";
			$sql .= "WHERE T1.mediaId = ".$mediaId;
			
			if($connection->IsDebug())
			{
				echo $sql;
			}
			
			$results = $connection->TabResSQL($sql);
			
			if(sizeof($results) > 0)
				return $results;
			else
				return false;
		}
		catch (MySQLExeption $e)
		{
			return $e->RetourneErreur();
		}
	}

	function RecupSaisons() {
		try
		{
			$connection = new Mysql(SQL_SERVER, SQL_DB, SQL_USERNAME, SQL_PASSWORD);
			
			$sql  = "SELECT id, libelle, isArchive, ordre ";
			$sql .= "FROM Saison ";
			$sql .= "ORDER BY ordre DESC ";
			
			if($connection->IsDebug())
			{
				echo $sql;
			}
			
			$results = $connection->TabResSQL($sql);
			if($connection->IsDebug())
			{
				echo sizeof($results);
			}
			
			if(sizeof($results) > 0)
				return $results;
			else
				return false;
		}
		catch (MySQLExeption $e)
		{
			if($connection->IsDebug())
			{
				echo $e->RetourneErreur();
			}
			return $e->RetourneErreur();
		}
	}
	
	function ajouterMedia($nom, $email, $libelle, $chemin, $type, $saisonId) {
		try
		{
			$connection = new Mysql(SQL_SERVER, SQL_DB, SQL_USERNAME, SQL_PASSWORD);
			$sql  = "INSERT INTO Media ";
			$sql .= "(libelle, chemin, saisonId, type, dateAjout) ";
			$sql .= "VALUES ";
			$sql .= "('".$libelle."', '".$chemin."', ".$saisonId.", ".$type.", now())";
			
			if($connection->IsDebug())
			{
				echo $sql;
			}
			
			$nbRows = $connection->ExecuteSQL($sql);
			return $nbRows;
		}
		catch (MySQLExeption $e)
		{
			return $e->RetourneErreur();
		}
	}
	
	function updateMediaStatus($mediaId, $visible) {
		try
		{
			$connection = new Mysql(SQL_SERVER, SQL_DB, SQL_USERNAME, SQL_PASSWORD);
			$sql  = "UPDATE Media ";
			$sql .= "SET visible = ".$visible." ";
			$sql .= "WHERE id = ".$mediaId;
			
			if($connection->IsDebug())
			{
				echo $sql;
			}
			
			$nbRows = $connection->ExecuteSQL($sql);
			return $nbRows;
		}
		catch (MySQLExeption $e)
		{
			return $e->RetourneErreur();
		}
	}
?>