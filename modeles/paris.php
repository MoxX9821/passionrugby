﻿<?php
	function get_all_matches_by_date($playerId, $date) {

		$infos = array();
		try {
			$connection = new Mysql(SQL_SERVER, SQL_DB, SQL_USERNAME, SQL_PASSWORD);
			
			$sql  = "SELECT `Match`.id, homeTeamId, awayTeamId, eventDate, IFNULL(`Pronostic`.homeScore, 0), IFNULL(`Pronostic`.awayScore, 0), groupId, Groupe.libelle AS groupe ";
			$sql .= "FROM `Match` INNER JOIN Groupe ON `Match`.groupId = Groupe.id ";
			$sql .= "LEFT JOIN `Pronostic` ON `Match`.id = `Pronostic`.matchId ";
			$sql .= "WHERE `Pronostic`.playerId=".$playerId." AND DATE(eventDate) = '".$date."' COLLATE latin1_general_ci";
			
			if($connection->IsDebug()) {
				echo $sql;
			}
			
			$results = $connection->TabResSQL($sql);
			
			$nbGames = sizeof($results);
			if($nbGames == 0) {
				return false;
			} else {
				for($i=0; $i<$nbGames; $i++) {
					
					$currentMatch = $results[$i];
					$homeTeamInfos = get_team_infos($currentMatch["homeTeamId"])[0];
					$awayTeamInfos = get_team_infos($currentMatch["awayTeamId"])[0];
					
					// Ajout du groupe
					$groupe = array($currentMatch[6], $currentMatch[7]);
					
					// Les infos du match
					$match = array($currentMatch["id"], date("H:i",strtotime($currentMatch["eventDate"])), 0, 0);
					
					// L'équipe à domicile
					$homeTeam = array($homeTeamInfos["id"], $homeTeamInfos["libelle"], "images/icons/".$homeTeamInfos["icon"]);
					array_push($match, $homeTeam);
					
					// L'équipe à l'extérieur
					$awayTeam = array($awayTeamInfos["id"], $awayTeamInfos["libelle"], "images/icons/".$awayTeamInfos["icon"]);
					array_push($match, $awayTeam);
					
					// On ajoute toutes les infos du match
					$currentInfosMatch = array();
					array_push($currentInfosMatch, $groupe);
					array_push($currentInfosMatch, $match);
					
					array_push($infos, date("d/m/Y",strtotime($date)));
					array_push($infos, $currentInfosMatch);
				}
			}
			
			return $infos;
		} catch (MySQLExeption $e) {
			return $e->RetourneErreur();
		}
	}
	
	function get_team_infos($id) {
		try {
			$connection = new Mysql(SQL_SERVER, SQL_DB, SQL_USERNAME, SQL_PASSWORD);
			
			$sql  = "SELECT id, libelle, icon ";
			$sql .= "FROM Team ";
			$sql .= "WHERE id = ".$id;
			
			if($connection->IsDebug())
			{
				echo $sql;
			}
			
			$results = $connection->TabResSQL($sql);
			
			return $results;
			
		} catch (MySQLExeption $e) {
			return $e->RetourneErreur();
		}
	}
	
	function update_scores($playerId, $scores) {
		try {
			
			$connection = new Mysql(SQL_SERVER, SQL_DB, SQL_USERNAME, SQL_PASSWORD);
			
			foreach($scores as $key => $val) {
				$gameId = $val[0];
				$homeScore = $val[1];
				$awayScore = $val[2];
				$isUpdate = $val[3];
				
				if($isUpdate) {				
					$sql  = "UPDATE Pronostic SET homeScore=".$homeScore.", awayScore=".$awayScore." WHERE playerId=".$playerId." AND matchId=".$gameId;
				} else {
					$sql  = "INSERT INTO Pronostic (playerId, matchId, homeScore, awayScore) VALUES (".$playerId.", ".$gameId.", ".$homeScore.", ".$awayScore.")";
				}
			}
			
			if($connection->IsDebug())
			{
				echo $sql;
			}
			
			$results = $connection->ExecuteSQL($sql);
			
			return $results;
			
		} catch (MySQLExeption $e) {
			return $e->RetourneErreur();
		}
	}
?>