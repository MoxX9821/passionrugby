<?php
	function maj_avatar_membre($id_utilisateur , $avatar) {

		$pdo = PDO2::getInstance();
		
		$requete = $pdo->prepare("UPDATE membres SET
			avatar = :avatar
			WHERE
			id = :id_utilisateur");

		$requete->bindParam(':id_utilisateur', $id_utilisateur);
		$requete->bindParam(':avatar', $avatar);

		return $requete->execute();
	}

	function valider_compte_avec_hash($hash_validation) {
		
		try
		{
			$connection = new Mysql(SQL_SERVER, SQL_DB, SQL_USERNAME, SQL_PASSWORD);
			
			$sql  = "UPDATE membres ";
			$sql .= "SET hash_validation = ''";
			$sql .= "WHERE hash_validation = '".$hash_validation."'";
			
			if($connection->IsDebug())
			{
				echo $sql;
			}
			
			$nbRows = $connection->ExecuteSQL($sql);
			return $nbRows == 1;
		}
		catch (MySQLExeption $e)
		{
			echo "Erreur : ". $e->RetourneErreur();
			return $e->RetourneErreur();
		}
	}

	function combinaison_connexion_valide($nom_utilisateur, $mot_de_passe) {
		
		try
		{
			$connection = new Mysql(SQL_SERVER, SQL_DB, SQL_USERNAME, SQL_PASSWORD);
			
			$sql  = "SELECT id  ";
			$sql .= "FROM membres ";
			$sql .= "WHERE nom_utilisateur = '".$nom_utilisateur."' AND mot_de_passe = '".$mot_de_passe."' AND hash_validation = ''";
			
			if($connection->IsDebug())
			{
				echo $sql;
			}
			
			$results = $connection->TabResSQL($sql);
			
			return (sizeof($results) == 1);
		}
		catch (MySQLExeption $e)
		{
			return $e->RetourneErreur();
		}
	}

	function lire_infos_utilisateur($id_utilisateur) {
		
		try
		{
			$connection = new Mysql(SQL_SERVER, SQL_DB, SQL_USERNAME, SQL_PASSWORD);
			
			$sql  = "SELECT nom_utilisateur, mot_de_passe, adresse_email, avatar, date_inscription, hash_validation  ";
			$sql .= "FROM membres ";
			$sql .= "WHERE id = :id_utilisateur = ".$id_utilisateur;
			
			if($connection->IsDebug())
			{
				echo $sql;
			}
			
			$results = $connection->TabResSQL($sql);
			
			if(sizeof($results) == 1)
				return	$results[0];
			else
				return false;
		}
		catch (MySQLExeption $e)
		{
			return $e->RetourneErreur();
		}
	}
?>