﻿													</div>
												</div>
											</div>
											<div class="wrapper-b1">
												<div class="wrapper-b2">
													<div class="wrapper-b3"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div id="left">
						<div class="module mod-box mod-box-header first ">
							<h3 class="header">
								<span class="header-2">
									<span class="header-3">Menu</span>
								</span>
							</h3>
							<div class="box-t1">
								<div class="box-t2">
									<div class="box-t3"></div>
								</div>
							</div>
							<div class="box-1">
								<div class="box-2">
									<div class="box-3 deepest with-header">
										<?php include "global/menu.php" ?>
									</div>
								</div>
							</div>
							<div class="box-b1">
								<div class="box-b2">
									<div class="box-b3"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="bottom">
				<div class="bottomblock width100 float-left">
					<div class="module mod-box mod-box-header first last">
						<div class="box-t1">
							<div class="box-t2">
								<div class="box-t3"></div>
							</div>
						</div>
						<div class="box-1">
							<div class="box-2">
								<div class="box-3 deepest ">
									<div style="text-align: center;">
										<span style="font-size: 90%;">
											Un texte de bas de page ici
										</span>
										<br>
										<span style="font-size: 90%;">
											Un autre ici
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="box-b1">
							<div class="box-b2">
								<div class="box-b3"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="footer">
				<a class="anchor" href="#page"></a>
			</div>
		</div>
	</body>
</html>