<?php
	$saisons = RecupSaisons();
	
	if($saisons)
	{
		$saisonCourante = $saisons[0];
	}
?>

<ul id="menu-accordeon">
	<li><a href="<?php echo $_SERVER['PHP_SELF']; ?>">Accueil</a></li>
	<li><a href="<?php echo $_SERVER['PHP_SELF']."?module=archives&action=saison&saison=".$saisonCourante["id"] ?>">Saison <?php echo $saisonCourante["libelle"]; ?></a></li>

<?php
	if(sizeof($saisons) > 1) {
?>
	<li><a href="#">Archives</a>
		<ul>
<?php
		for($i=1; $i<sizeof($saisons); $i++) {
			$saison = $saisons[$i];
			echo "<li><a href=".$_SERVER['PHP_SELF']."?module=archives&action=saison&saison=".$saison["id"].">Saison ".$saison["libelle"]."</a></li>";
		}
?>
		</ul>
<?php
	}
?>
	</li>
</ul>