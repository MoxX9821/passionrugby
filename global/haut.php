﻿<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
	<head>
		<meta charset="utf-8">
		<title>RCU - Passion Rugby</title>
		<meta http-equiv="Content-Language" content="fr" />
		<!-- Les styles -->
		<link type="text/css" href="css/general.css" rel="stylesheet" />
		<link type="text/css" href="css/header.css" rel="stylesheet" />
		<link type="text/css" href="css/content.css" rel="stylesheet" />
		<link type="text/css" href="css/left.css" rel="stylesheet" />
		<link type="text/css" href="css/menu.css" rel="stylesheet" />
		<link type="text/css" href="css/bas.css" rel="stylesheet" />
		<link type="text/css" href="css/error.css" rel="stylesheet" />
		
		<script type="text/javascript" src="js/saison.js"></script>
		
		<link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
		<link href="css/demo.css" rel="stylesheet" />
		<link href="css/avgrund.css" rel="stylesheet" />
		<script type="text/javascript" src="js/avgrund.js"></script>
		<script>
			function openDialog(id) {
				Avgrund.show("#"+id);
			}
			function closeDialog() {
				Avgrund.hide();
			}
		</script>
	</head>
	<body>
		<div class="wrapper">
			<div id="header">
				<div class="header-1">
					<div id="headerbar">
						<div class="logo">
							<img src="./images/rcu.png" alt="logo du RCU" title="Logo du RCU" />
						</div>
						<div class="title">
							<p>Rubgy Club Uz&egrave;s</p>
						</div>
					</div>
				</div>
			</div>
			<div id="middle">
				<div id="middle-expand">
					<div id="main">
						<div id="main-shift">	
							<div id="mainmiddle">
								<div id="mainmiddle-expand">
									<div id="content">
										<div id="content-shift">
											<div class="wrapper-t1">
												<div class="wrapper-t2">
													<div class="wrapper-t3"></div>
												</div>
											</div>
											<div class="wrapper-1">
												<div class="wrapper-2">
													<div class="wrapper-3">